<?php

class Shopping_Model extends CI_Model {
    
    protected $table = 'shopping';

    protected $primaryKey = 'id';

    public function get()
    {
        $this->db->select('shopping.id, shopping.name, shopping.createddate');
        $this->db->from($this->table);
        return $this->db->get();
    }
    public function filter($filter) 
    {
        $this->db->select('shopping.id, shopping.name, shopping.createddate');
        $this->db->from($this->table);
        $this->db->where($filter);

        return $this->db->get();
    }

    public function save($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }

    public function delete($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->table);
    }

}
