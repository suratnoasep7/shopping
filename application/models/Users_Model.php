<?php

class Users_Model extends CI_Model {
    
    protected $table = 'user';

    protected $primaryKey = 'id';

    public function get()
    {
        $this->db->select('user.id, user.username, user.email, user.phone, user.country, user.city, user.postcode, user.name, user.address, user.postcode');
        $this->db->from($this->table);
        return $this->db->get();
    }

    public function findByEmail($email)
    {
        $this->db->select('user.id, user.username, user.email, user.password');
        $this->db->from($this->table);
        $this->db->where(['user.email' => $email]);
        return $this->db->get();
    }

    public function filter($filter) 
    {
        $this->db->select('user.id, user.email, user.username');
        $this->db->from($this->table);
        $this->db->where($filter);

        return $this->db->get();
    }

    public function findByToken($token) 
    {
        return $this->db->get_where($this->table, ['access_token' => $token]);
    }

    public function save($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }

    public function delete($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }

}
