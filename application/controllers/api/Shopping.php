<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Shopping extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Shopping_Model']);
    }

    public function index_get($id = null) 
    {

        $authHeader = $this->input->get_request_header('Authorization');
 
        $arr = explode("Bearer ", $authHeader);
 
        $token = $arr[1];

        if(empty($token)) {
            return $this->messageUnAthorized();
        }

        $decodedToken = AUTHORIZATION::checkToken($token)->num_rows();

        if($decodedToken == 0) {
            return $this->messageError();
        }
        $dataShopping = $this->Shopping_Model->get()->result_array();
        $response = array('data' => $dataShopping);
        return $this->set_response($response, REST_Controller::HTTP_OK);

        
    }

    public function index_post() 
    {
        $authHeader = $this->input->get_request_header('Authorization');
 
        $arr = explode("Bearer ", $authHeader);
 
        $token = $arr[1];

        if(empty($token)) {
            return $this->messageUnAthorized();
        }

        $decodedToken = AUTHORIZATION::checkToken($token)->num_rows();

        if($decodedToken == 0) {
            return $this->messageError();
        }

        $data = array();
        $obj = file_get_contents('php://input');
        $dataShopping = json_decode($obj);
        $shopping = $dataShopping->shopping;
        if($this->insertDataShopping($shopping)) {
            $shopping = $this->Shopping_Model->filter(['shopping.id' => $this->db->insert_id()])->row();
            $data['message'] = 'SUCCESS';
            $data['status'] = true;
            $data['createddate'] = $shopping->createddate;
            $data['id'] = $shopping->id;
            $data['name'] = $shopping->name;
            $this->set_response($data, REST_Controller::HTTP_OK);
        } else {
            $data = array();
            $data['message'] = 'ISI Data Dengan Baik Dan Benar';
            $data['data'] = [];
            $data['status'] = false;
            $this->set_response($data, REST_Controller::HTTP_BAD_REQUEST);
        }
        
    }

    public function index_put() 
    {
        $data = array();
        $obj = file_get_contents('php://input');
        $dataShopping = json_decode($obj);
        $shopping = $dataShopping->shopping;
        if($this->updateDataShopping($shopping)) {
            $shopping = $this->Shopping_Model->filter(['shopping.id' => $this->put('id')])->row();
            $data['message'] = 'SUCCESS';
            $data['status'] = true;
            $data['createddate'] = $shopping->createddate;
            $data['id'] = $shopping->id;
            $data['name'] = $shopping->name;
            $this->set_response($data, REST_Controller::HTTP_OK);
        } else {
            $data = array();
            $data['message'] = 'ISI Data Dengan Baik Dan Benar';
            $data['data'] = [];
            $data['status'] = false;
            $this->set_response($data, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_delete() 
    {
        
        if($this->deleteDataShopping()) {
            $data = array();
            $data['message'] = 'SUCCESS';
            $data['status'] = true;
            $data['data'] = [];
            $this->set_response($data, REST_Controller::HTTP_OK);
        } else {
            $data = array();
            $data['message'] = 'ISI Data Dengan Baik Dan Benar';
            $data['data'] = [];
            $data['status'] = false;
            $this->set_response($data, REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    public function insertDataShopping($shopping) 
    {
        
        $data = [
            'name' => $shopping->name,
            'createddate' => $shopping->createddate
        ];
        $response = $this->Shopping_Model->save($data);
        return $response;
    }

    private function updateDataShopping($shopping) 
    {
        $id = $this->put('id');
        $data = [
            'name' => $shopping->name,
            'createddate' => $shopping->createddate
        ];
        $response = $this->Shopping_Model->update($id, $data);
        return $response;
    }

    private function deleteDataShopping() 
    {
        $id = $this->delete('id');
        $response = $this->Shopping_Model->delete($id);
        return $response;
    }
    private function messageUnAthorized() 
    {
        $data = array();
        $data['message'] = 'Data Tidak Ditemukan';
        $data['data'] = [];
        $data['status'] = false;
        $this->set_response($data, REST_Controller::HTTP_UNAUTHORIZED);
    }

    private function messageError() 
    {
        $data = array();
        $data['message'] = 'ISI Data Dengan Baik Dan Benar';
        $data['data'] = [];
        $data['status'] = false;
        $this->set_response($data, REST_Controller::HTTP_BAD_REQUEST);
    }
}