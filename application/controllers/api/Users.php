<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Users extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Users_Model']);
    }

    public function index_get() 
    {

        $authHeader = $this->input->get_request_header('Authorization');
 
        $arr = explode("Bearer ", $authHeader);
 
        $token = $arr[1];

        if(empty($token)) {
            return $this->messageUnAthorized();
        }

        $decodedToken = AUTHORIZATION::checkToken($token)->num_rows();

        if($decodedToken == 0) {
            return $this->messageError();
        }

        $dataUsers = $this->Users_Model->get()->result_array();
        $response = array('data' => $dataUsers);
        return $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function signup_post() 
    {
        $data = array();
        $obj = file_get_contents('php://input');
        $dataSignUp = json_decode($obj);
        $dataUser = $dataSignUp->user;
        if($this->insertDataSignup($dataUser)) {
            $user = $this->Users_Model->filter(['user.id' => $this->db->insert_id()])->row();
            $data['message'] = 'SUCCESS';
            $data['status'] = true;
            $data['email'] = $user->email;
            $tokenData = array();
            $tokenData['id'] = $user->id;
            $token = AUTHORIZATION::generateToken($tokenData);
            $data['token'] = $token;
            $data['username'] = $user->username;
            $this->set_response($data, REST_Controller::HTTP_OK);
        } else {
            $data = array();
            $data['message'] = 'ISI Data Dengan Baik Dan Benar';
            $data['data'] = [];
            $data['status'] = false;
            $this->set_response($data, REST_Controller::HTTP_BAD_REQUEST);
        }
        
    }

    public function signin_post() 
    {
        $obj = file_get_contents('php://input');
        $dataSignIn = json_decode($obj);
        $dataUsers = $this->Users_Model->findByEmail($dataSignIn->email);

        if($dataUsers->num_rows() > 1) {
            $this->messageDataNotFound();
        }

        if(password_verify($dataSignIn->password, $dataUsers->row()->password)) {
            $data['message'] = 'SUCCESS';
            $data['status'] = true;
            $data['email'] = $dataUsers->row()->email;

            $tokenData = array();
            $tokenData['id'] = $dataUsers->row()->id;
            $token = AUTHORIZATION::generateToken($tokenData);
            $dataUpdate = [
                'access_token' => $token
            ];
            $this->Users_Model->update($dataUsers->row()->id, $dataUpdate);
            $data['token'] = $token;
            $data['username'] = $dataUsers->row()->username;
            $this->set_response($data, REST_Controller::HTTP_OK);
        } else {
            $data = array();
            $data['message'] = 'ISI Data Dengan Baik Dan Benar';
            $data['data'] = [];
            $data['status'] = false;
            $this->set_response($data, REST_Controller::HTTP_BAD_REQUEST);    
        }
    }

    public function insertDataSignup($user) 
    {
        $options = ['cost' => 10];
        $password = "";
        if(empty($user->encrypted_password)) {
            $password = password_hash("admin", PASSWORD_DEFAULT, $options);
        } else {
            $password = password_hash($user->encrypted_password, PASSWORD_DEFAULT, $options);
        }
        $data = [
            'username' => $user->username,
            'email' => $user->email,
            'password' => $password,
            'phone' => $user->phone,
            'address' => $user->address,
            'city' => $user->city,
            'country' => $user->country,
            'name' => $user->name,
            'postcode' => $user->postcode
        ];
        $response = $this->Users_Model->save($data);
        return $response;
    }
    private function messageUnAthorized() 
    {
        $data = array();
        $data['message'] = 'Data Tidak Ditemukan';
        $data['data'] = [];
        $data['status'] = false;
        $this->set_response($data, REST_Controller::HTTP_UNAUTHORIZED);
    }

    private function messageError() 
    {
        $data = array();
        $data['message'] = 'ISI Data Dengan Baik Dan Benar';
        $data['data'] = [];
        $data['status'] = false;
        $this->set_response($data, REST_Controller::HTTP_BAD_REQUEST);
    }
}